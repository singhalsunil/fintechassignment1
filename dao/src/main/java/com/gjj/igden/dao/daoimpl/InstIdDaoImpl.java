package com.gjj.igden.dao.daoimpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.InstId;

@Repository
@SuppressWarnings("unchecked")
public class InstIdDaoImpl extends AbstractDAO<InstId> {

	public void create(InstId instId) throws DAOException {
		super.create(instId);
	}
	@Override
	public InstId read(InstId obj) {
		return (InstId) em.createQuery("from InstId where instId = "+obj.getInstIdKey().getId()).getSingleResult();
	}

	
	@Override
	public List<InstId> readAll() {
		return em.createQuery("from InstId").getResultList();
	}
	
	public boolean delete(InstId instId) throws DAOException {
		return super.delete(read(instId));
	}
	
	public void delete(String instId) {
		em.createQuery("delete from InstId where instId="+instId);
	}
	
	public void updateInstIdToNull(long watchListId_fk) {
		em.createQuery("update InstId set iWatchListDesc = 0 where watchlist_id_fk="+watchListId_fk).executeUpdate();
	}
	
	public List<String> searchTickersByChars(String tickerNamePart) {
		List<InstId> instids = em.createQuery("FROM InstId i where i.instId LIKE '%"+tickerNamePart+"%'").getResultList();
		return instids.stream().map(instid -> instid.getInstIdKey().getId()).collect(Collectors.toList());
	}

}
