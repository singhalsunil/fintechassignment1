package com.gjj.igden.dao.daoimpl;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class WatchListDescDaoImpl extends AbstractDAO<IWatchListDesc> {

	public WatchListDesc read(WatchListDesc obj) {
		WatchListDesc wl = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where wl.watchListDescKey.watchListId ="+obj.getWatchListDescKey().getWatchListId()).getSingleResult();
		System.out.println("WatchListDescDaoImpl.read():::::"+wl.getWatchListId());
		return wl;
	}
	
	public void deleteWatchList(WatchListDesc obj) {
		em.createQuery("DELETE FROM WatchListDesc wl where "
				+ "wl.watchListDescKey.watchListId ="
				+obj.getWatchListDescKey().getWatchListId()).
				executeUpdate();
	}
	
	public WatchListDesc readByWatchListIdAndAccountId(Long watchListId, Long accountId) {
		WatchListDesc wl = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where wl.watchListDescKey.watchListId ="+watchListId+" AND wl.watchListDescKey.accountId = "+accountId).getSingleResult();
		System.out.println("WatchListDescDaoImpl.readByWatchListIdAndAccountId():::"+wl.getWatchListId());
		return wl;
	}

	@Override
	public List<IWatchListDesc> readAll() {
		return em.createQuery("FROM WatchListDesc").getResultList();
	}
	
	public boolean createWatchListDesc(WatchListDesc watchListDesc) throws DAOException {
		Long max = getMaxIdFromDatabase();
		if(null == getMaxIdFromDatabase()) {
			watchListDesc.getWatchListDescKey().setWatchListId(1L);
		} else {
			watchListDesc.getWatchListDescKey().setWatchListId(max+1);
		}
		super.create(watchListDesc);
		return true;
	}
	
	public List<WatchListDesc> getDataSetsAttachedToAcc(Long id) {
		return em.createQuery("FROM WatchListDesc WHERE account_fk_id = "+id).getResultList();
	}

	public List<String> getAllStockSymbols(Long id) {
		WatchListDesc watchListDesc = (WatchListDesc) em.createQuery("FROM WatchListDesc where wl.watchListDescKey.watchListId="+id).getSingleResult();
		List<String> list = em.createQuery("select instId FROM InstId where watchlist_id_fk="+watchListDesc.getId()).getResultList();
		
		return list;
	}
	
	public boolean deleteWatchListDesc(Long dsId, Long accId) throws DAOException {
		WatchListDesc wl = readByWatchListIdAndAccountId(dsId, accId);
		//em.remove(em.contains(wl) ? wl : em.merge(wl));
		em.remove(em.getReference(WatchListDesc.class, wl)); 
		return true;
	}
	
	public IWatchListDesc getWatchListDesc(Long dsId, Long accId) {
		return (IWatchListDesc) em.createQuery("FROM WatchListDesc WHERE id = "+dsId+" AND Account = "+new Account(accId)).getSingleResult();
	}
	
	public boolean deleteWatchListDesc(WatchListDesc watchListDesc) throws DAOException {
		deleteWatchList(watchListDesc);
		return true;
	}
	
	public boolean updateWatchListDesc(IWatchListDesc watchListDesc) {
		super.update(watchListDesc);
		return false;
	}
	
	public Long getMaxIdFromDatabase() {
		Long max = (Long) em.createQuery("select MAX(wl.watchListDescKey.watchListId) FROM WatchListDesc wl").getSingleResult();
		System.out.println("WatchListDescDaoImpl.getMaxIdFromDatabase()"+max);
		return max;
	}

	@Override
	public IWatchListDesc read(IWatchListDesc obj) {
		return null;
	}
	

}
