package com.gjj.igden.dao.daoimpl;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.model.Bar;
import com.gjj.igden.model.InstId;
import com.google.common.base.Predicate;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class BarDaoImpl extends AbstractDAO<Bar> {
  
	@Override
	public Bar read(Bar obj) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public List<Bar> readAll() {
		return em.createQuery("from Bar").getResultList();
	}
	
	public Bar getSingleBar(long id, String instId) {

		return (Bar) em.createQuery("FROM Bar WHERE id = "+id+" AND instId_fk = '"+instId+"' ").getSingleResult();
	}
  
	public Set<Bar> getBarList(String instId) {
		List<Bar> bar1 = em.createQuery("FROM Bar WHERE instId_fk = '"+instId+"' ").getResultList();
		Set<Bar> bar2 = new HashSet<>();
		for(Bar bar:bar1) {
				bar2.add(bar);
		}
		return bar2;
	}
  
  
	public boolean createBar(Bar bar) throws DAOException {
		super.create(bar);
		return true;
	}
	
	public boolean updateBar(Bar bar) {
		super.update(bar);
		return true;
	}
	
	public boolean deleteBar(long mdId, String instId) {
		em.createQuery("DELETE FROM Bar WHERE id = "+mdId+" AND instId_fk = '"+instId+"' ").executeUpdate();
		return true;
	}
	
	public boolean deleteBar(Bar bar) throws DAOException {
		super.delete(bar);
		return true;
	}
	
	public void deleteBarByInstId(InstId instId) {
		
	}
	
/*	public List<String> searchTickersByChars(String tickerNamePart) {
		List<Bar> bars = em.createQuery("FROM Bar b where b.instId LIKE '%"+tickerNamePart+"%'").getResultList();
		return bars.stream().map(bar -> bar.getInstId().getInstId()).collect(Collectors.toList());
	}*/
	
	public List<Bar> getFullTickersObjectByChars(String tickerNamePart) {
		List<Bar> list = em.createQuery("FROM Bar b where b.instId LIKE '%"+tickerNamePart+"%'").getResultList();
		return list.stream().collect(Collectors.toList());
	}
}
