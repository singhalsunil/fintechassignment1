package com.gjj.igden.dao.daoimpl;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.model.Role;

import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;

import java.util.List;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class RoleDaoImpl extends AbstractDAO<Role> {

	@Override
	public Role read(Role obj) {
		// TODO Auto-generated method stub
		return (Role) em.createQuery("from Role where id = "+obj.getId()).getSingleResult();
	}

	
	@Override
	public List<Role> readAll() {
		return em.createQuery("from Role").getResultList();
	}
	
}