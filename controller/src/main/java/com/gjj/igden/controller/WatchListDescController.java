package com.gjj.igden.controller;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.model.WatchListDescKey;
import com.gjj.igden.service.accountService.AccountService;
import com.gjj.igden.service.watchlist.WatchListDescService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
public class WatchListDescController {
	
	static Logger logger = LogManager.getLogger(GreetingController.class);
	
  @Autowired
  private WatchListDescService service;//renamed WatchListDescServiceService to WatchListDescService 
  
  @Autowired
  private AccountService accountService;
  
  @RequestMapping(value = "/view-watchlist", method = RequestMethod.GET)
  public String viewWatchList(ModelMap model, @RequestParam Long watchListId) {
	  logger.debug("Viewing watch-list for watchlist id::" + watchListId);
    model.addAttribute("stockSymbolsList", service.getStockSymbolsList(watchListId));
    model.addAttribute("watchListId", watchListId);
    return "view-watchlist";
  }

  @GetMapping(value = "/add-watchlist")
  public String addWatchList(ModelMap model, @RequestParam Long id) {
	  logger.debug("Adding watch-list for watchlist id::" + id);
    WatchListDesc theWatchListDesc = new WatchListDesc();
    theWatchListDesc.setWatchListDescKey(new WatchListDescKey(accountService.getAccount(id)));
    System.out.println("WatchListDescController.addWatchList():::::"+theWatchListDesc.getWatchListDescKey().getAccountId());
    model.addAttribute("theWatchListDesc", theWatchListDesc);
    return "lazyRowLoad";
  }

  @PostMapping(value = "/lazyRowAdd.web")
  public String lazyRowAdd(@ModelAttribute("theWatchListDesc") WatchListDesc theWatchListDesc, 
		  @RequestParam("acc_id") Long accId) throws DAOException {
	  logger.debug("Lazy-row loading for WatchListDesc::" + theWatchListDesc + "accid" + accId);
    service.create(theWatchListDesc, accId);
    return "redirect:/view-account?id="+accId;
  }
  
  @RequestMapping(value = "/delete-watchlist", method = RequestMethod.GET)
  public String deleteAccount(ModelMap model, @RequestParam("watchListId") Long watchListId, 
		  @RequestParam("accountId") Long accountId) {
	  logger.debug("Deleting watchlist for id::" + watchListId);
    try {
		service.delete(watchListId, accountId);
	} catch (DAOException e) {
		 logger.error("Error while deleting watchlist for id::" + e);
		e.printStackTrace();
	}
    return "redirect:/view-account?id="+accountId;
  }
  
}
