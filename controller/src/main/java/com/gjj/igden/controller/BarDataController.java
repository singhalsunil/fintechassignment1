package com.gjj.igden.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gjj.igden.model.Bar;
import com.gjj.igden.service.barService.BarService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Set;

@Controller
public class BarDataController {
	
	static Logger logger = LogManager.getLogger(BarDataController.class);
	
  @Autowired
  private BarService service;

  @RequestMapping(value = "/view-data", method = RequestMethod.GET)
  public String viewBarData(ModelMap model, @RequestParam String stockSymbol) {
	  logger.debug("Viewing bar data for stockSymbol::" + stockSymbol);
    Set<Bar> barList = service.getBarList(stockSymbol);
    model.addAttribute("barData", barList);
    return "view-data";
  }

  @RequestMapping(value = "/search", method = RequestMethod.GET)
  public String searchGet1() {
	  logger.debug("Viewing bar data for stockSymbol::");
    return "search";
  }

  @RequestMapping(value = "/ajax_search", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> searchGet2(@RequestParam String searchParam) {
    List<String> tickets = service.searchTickersByChars(searchParam);
    String result = "";
    try {
		result = writeListToJsonArray(tickets);
		logger.debug("Ajax autocomplete search result::" + result);
	} catch (IOException e) {
		logger.error("Error in Ajax autocomplete search ::" + e);
		e.printStackTrace();
	}
    return new ResponseEntity<String>("{\"item\":"+result+"}", HttpStatus.OK) ;
  }
  
  @RequestMapping(value = "/DataController", method = RequestMethod.GET)
  public String searchGet(ModelMap model, @RequestParam("myText") String myText) {
	  logger.debug("Fetching bar list for symbol::" + myText);
    List<Bar> bars = service.getFullTickersObjectByChars(myText);
    model.addAttribute("THE_SEARCH_RESULT_LIST", bars);
    return "search";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchPost(ModelMap model, @RequestParam String stockSymbol) {
	  logger.debug("Fetching bar list for symbol::" + stockSymbol);
    Set<Bar> barList = service.getBarList(stockSymbol);
    model.addAttribute("barData", barList);
    return "view-data";
  }
  
  public String writeListToJsonArray(List<String> list) throws IOException {  
	    final OutputStream out = new ByteArrayOutputStream();
	    final ObjectMapper mapper = new ObjectMapper();

	    mapper.writeValue(out, list);
	    return out.toString();
	}
  
  public String writeListToJsonArraySet(List<MyValue> list) throws IOException {
	    final OutputStream out = new ByteArrayOutputStream();
	    final ObjectMapper mapper = new ObjectMapper();

	    mapper.writeValue(out, list);
	    return out.toString();
	}
  
  private class MyValue{
	  private String value;
	  
	  public MyValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	  
  }
 
}
