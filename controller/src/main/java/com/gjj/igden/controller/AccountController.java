package com.gjj.igden.controller;

import com.gjj.igden.dao.daoimpl.RoleDaoImpl;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.Role;
import com.gjj.igden.service.accountService.AccountService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class AccountController {
	
	static Logger logger = LogManager.getLogger(AccountController.class);

	@Autowired
	private AccountService service;

	@Autowired
	private RoleDaoImpl roleDaoImpl;

	@RequestMapping(value = "/admin/list-accounts", method = RequestMethod.GET)
	public String showAccounts(ModelMap model) {
		logger.debug("Reteriving list of Account");
		model.addAttribute("ACCOUNT_LIST", service.getAccountList());
		return "list-accounts";
	}

	@RequestMapping(value = "/edit-account", method = RequestMethod.POST)
	public String putEditedAccountToDb(Account account, RedirectAttributes redirectAttributes) {
		logger.debug("Editing Account for id::" + account.getId());
		boolean editStatus = service.updateAccount(account);
		if (editStatus) {
			redirectAttributes.addAttribute("success", "true");
		} else {
			System.err.println("editing failed");
		}
		return "redirect:/admin/list-accounts";
	}

	@RequestMapping(value = "/add-account", method = RequestMethod.GET)
	public String createNewAccountGet(ModelMap model) {
		logger.debug("Creating an Account");
		model.addAttribute("accountRoles", roleDaoImpl.readAll());
		model.addAttribute("account", new Account());
		return "add-account";
	}

	@RequestMapping(value = "/processAddAccount1", method = RequestMethod.POST)
	public String createAccountPost1(@RequestParam(value = "username1", required = false) String username1) {
		logger.debug("Saving username ::in createAccountPost1::" + username1);
		Account account = new Account();
		account.setAccountName(username1);
		service.createAccount(account);
		return "redirect:/list-accounts";
	}

	@RequestMapping(value = "/processAddAccount", method = RequestMethod.POST)
	public String createAccountPost2(@RequestParam(value = "username1", required = false) String username1) {
		logger.debug("Saving username::in createAccountPost2::" + username1);
		Account account = new Account();
		account.setAccountName(username1);
		service.createAccount(account);
		return "redirect:/list-accounts";
	}

	@RequestMapping(value = "/add-account", method = RequestMethod.POST)
	public String createAccountPost(Account account, @RequestParam(value = "role", required = false) String role) {
		logger.debug("Saving role and account::in createAccountPost::" + account);
		account.addRole(roleDaoImpl.read(new Role(Long.parseLong(role))));
		service.createAccount(account);
		return "redirect:/admin/list-accounts";
	}

	@RequestMapping(value = "/delete-account", method = RequestMethod.GET)
	public String deleteAccount(@RequestParam Long id) {
		logger.debug("Deleting account for id::" + id);
		boolean status = false;
		try {
			status = service.delete(id);
		} catch (Exception e) {
			logger.error("Error while deleting account for id::" + id + e);
			status = false;
		}
		if (status) {
			return "redirect:/admin/list-accounts";
		} else {
			return "errorPage";
		}
	}

	@RequestMapping(value = "/edit-account", method = RequestMethod.GET)
	public String getAccountToEditAndPopulateForm(ModelMap model, @RequestParam Long id) {
		logger.debug("Editing account for id:", + id);	
		Account account = service.retrieveAccount(id);
		logger.debug(String.format("Retrieving account for id::"),account);
		model.addAttribute("account", account);
		return "edit-account";
	}

	@RequestMapping(value = "/getImage", method = RequestMethod.GET)
	@ResponseBody
	public byte[] showImage(@RequestParam("accId") int itemId/* , HttpServletResponse response */)
			throws ServletException, IOException {
		logger.debug("Retrieving image for itemid",itemId);
		byte[] buffer = service.getImage(itemId);
		return buffer;
	}

	@PostMapping("/uploadImage") // new annotation since 4.3 todo make this new annotation everywhere
	public String setNewImage(@RequestParam("image") MultipartFile imageFile, RedirectAttributes redirectAttributes,
			Account account) {
		logger.debug("Uploading image for account" + account);
		if (imageFile.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			return "redirect:uploadStatus";
		}
		try {
			byte[] bytes = imageFile.getBytes();
			InputStream imageConvertedToInputStream = new ByteArrayInputStream(bytes);
			service.setImage(account.getId(), imageConvertedToInputStream);
			redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + imageFile.getOriginalFilename() + "'");
			logger.debug("Image uploaded for account" + account);
		} catch (IOException e) {
			logger.error("Error in Image uploading for account" + account);
			e.printStackTrace();
		}
		return "redirect:/uploadStatus";
	}

	@GetMapping("/uploadStatus")
	public String uploadStatus() {
		logger.debug("Image uploaded successfully");
		return "uploadStatus";
	}

	@RequestMapping(value = "/view-account", method = RequestMethod.GET)
	public String viewAccount(ModelMap model, @RequestParam Long id) {
		logger.debug("Viewing account for id" + id);
		Account account = service.retrieveAccount(id);
		model.addAttribute("watchLists", account.getAttachedWatchedLists());
		model.addAttribute("account", account);
		return "view-account";
	}

	@RequestMapping(value = "/home")
	public String home() {
		logger.debug("Redirecting to home page");
		return "/other-jsp/home";
	}

	@RequestMapping(value = "/location")
	public String location() {
		logger.debug("Redirecting to location");
		return "/other-jsp/location";
	}

}
