package com.gjj.igden.controller;

import com.gjj.igden.model.Greeting;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class GreetingController {

	static Logger logger = LogManager.getLogger(GreetingController.class);
	
  @GetMapping("/greeting")
  public String greetingForm(Model model) {
	  logger.debug("Fetching greetings");
    model.addAttribute("greeting", new Greeting());
    return "greeting";
  }

  @RequestMapping(value = "/greeting", method = RequestMethod.POST)
    public String greetingSubmit(@ModelAttribute Greeting greeting) {
	  logger.debug("Submitting greetings::" + greeting.getContent());
    return "result";
  }

}

