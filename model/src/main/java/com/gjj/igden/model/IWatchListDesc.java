package com.gjj.igden.model;

import java.util.List;

import com.gjj.igden.utils.EntityId;

public interface IWatchListDesc extends EntityId {
  List<String> getStockSymbolsList();

  void setStockSymbolsList(List<String> stockSymbolsList);

  void setStockSymbolsListFromOperationList(List<OperationParameters> stockSymbolsList);

  Long getWatchListId();

  void setWatchListId(Long watchListId);

  String getWatchListName();

  void setWatchListName(String watchListName);

  String getWatchListDetails();

  void setWatchListDetails(String watchListDetails);

  int getMarketDataFrequency();

  void setMarketDataFrequency(int marketDataFrequency);

  String getDataProviders();

  void setDataProviders(String dataProviders);

  String toString();

  List<OperationParameters> getOperationParameterses();

  void setOperationParameterses(List<OperationParameters> operationParameterses);

  public WatchListDescKey getWatchListDescKey();
  
  public void setWatchListDescKey(WatchListDescKey watchListDescKey);
}
