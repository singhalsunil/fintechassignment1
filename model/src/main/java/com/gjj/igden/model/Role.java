package com.gjj.igden.model;

import org.springframework.security.core.GrantedAuthority;

import com.gjj.igden.utils.EntityId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "app_role")
public class Role implements GrantedAuthority, EntityId {

	private static final long serialVersionUID = 6997624284483518680L;

	private Long id;

	private String name;

	public Role() {
	}

	public Role(Long id) {
		super();
		this.id = id;
	}

	@Override
	@Id
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	@Transient
	public String getAuthority() {
		return getName();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

}
