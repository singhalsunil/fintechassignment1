package com.gjj.igden.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class WatchListDescKey implements Serializable {

	private static final long serialVersionUID = -1060335893865635944L;

	@Column(name = "data_set_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long watchListId;

	@Column(name = "account_fk_id")
	private Long accountId;

	public WatchListDescKey() {
	}

	public WatchListDescKey(Long accountId) {
		this.accountId = accountId;
	}
	
	public WatchListDescKey(Account account) {
		this.accountId = account.getId();
	}

	public WatchListDescKey(Long watchListId, Long accountId) {
		super();
		this.watchListId = watchListId;
		this.accountId = accountId;
	}

	public Long getWatchListId() {
		return watchListId;
	}

	public void setWatchListId(Long watchListId) {
		this.watchListId = watchListId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
		result = prime * result + ((watchListId == null) ? 0 : watchListId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WatchListDescKey other = (WatchListDescKey) obj;
		if (accountId == null) {
			if (other.accountId != null)
				return false;
		} else if (!accountId.equals(other.accountId))
			return false;
		if (watchListId == null) {
			if (other.watchListId != null)
				return false;
		} else if (!watchListId.equals(other.watchListId))
			return false;
		return true;
	}

}