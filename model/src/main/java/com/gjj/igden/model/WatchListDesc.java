package com.gjj.igden.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections4.FactoryUtils;
import org.apache.commons.collections4.list.LazyList;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "data_set")
public class WatchListDesc implements IWatchListDesc, Serializable {

	@EmbeddedId
	private WatchListDescKey watchListDescKey;
	
	@ManyToOne
	@MapsId("account_fk_id")
	@JoinColumn(name = "account_fk_Id", referencedColumnName = "account_Id")
	private Account account;
	
	private String watchListName;
	private String watchListDetails;
	private int marketDataFrequency;
	private String dataProviders;
	
	@OneToMany(mappedBy = "watchListDesc")
	@Cascade(CascadeType.ALL)
	private List<InstId> instIdList;
	
	@Transient
	private List<String> stockSymbolsList;
	
	@Transient
	private List<OperationParameters> operationParameterses = LazyList.lazyList(new ArrayList<>(),
			FactoryUtils.instantiateFactory(OperationParameters.class));
	
	
	public WatchListDesc() {
	}

	public WatchListDesc(Account account) {
		this.account = account;
		this.watchListDescKey = new WatchListDescKey(account.getId());
	}
	
	public WatchListDesc(WatchListDescKey watchListDescKey, Long watchListId, Account account, String watchListName,
			String watchListDetails, int marketDataFrequency, String dataProviders, List<String> stockSymbolsList,
			List<OperationParameters> operationParameterses, Set<com.gjj.igden.model.InstId> instIds) {
		super();
		this.watchListDescKey = watchListDescKey;
		watchListDescKey = new WatchListDescKey(account);
		this.account = account;
		this.watchListName = watchListName;
		this.watchListDetails = watchListDetails;
		this.marketDataFrequency = marketDataFrequency;
		this.dataProviders = dataProviders;
		this.stockSymbolsList = stockSymbolsList;
		this.operationParameterses = operationParameterses;
	}
	
	public WatchListDesc(Long watchListId) {
		this.watchListDescKey = new WatchListDescKey();
		this.watchListDescKey.setWatchListId(watchListId);
	}
	
	

	public List<String> getStockSymbolsList() {
		return stockSymbolsList;
	}

	public void setStockSymbolsList(List<String> stockSymbolsList) {
		this.stockSymbolsList = stockSymbolsList;
	}

	public void setStockSymbolsListFromOperationList(List<OperationParameters> stockSymbolsList) {
		List<String> stringList = stockSymbolsList.stream().map(OperationParameters::getName)
				.collect(Collectors.toList());
		this.stockSymbolsList = stringList;
	}

	public List<OperationParameters> getOperationParameterses() {
		return operationParameterses;
	}

	public void setOperationParameterses(List<OperationParameters> operationParameterses) {
		this.operationParameterses = operationParameterses;
	}

	public Long getWatchListId() {
		return watchListDescKey.getWatchListId();
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
		this.watchListDescKey.setAccountId(account.getId());
	}

	public String getWatchListName() {
		return watchListName;
	}

	public void setWatchListName(String watchListName) {
		this.watchListName = watchListName;
	}

	public String getWatchListDetails() {
		return watchListDetails;
	}

	public void setWatchListDetails(String watchListDetails) {
		this.watchListDetails = watchListDetails;
	}

	public int getMarketDataFrequency() {
		return marketDataFrequency;
	}

	public void setMarketDataFrequency(int marketDataFrequency) {
		this.marketDataFrequency = marketDataFrequency;
	}

	public String getDataProviders() {

		// TODO m it should be List<providerID> like phone in social network
		return dataProviders;
	}

	public void setDataProviders(String dataProviders) {
		this.dataProviders = dataProviders;
	}

	public WatchListDescKey getWatchListDescKey() {
		return watchListDescKey;
	}

	public void setWatchListDescKey(WatchListDescKey watchListDescKey) {
		this.watchListDescKey = watchListDescKey;
	}

	@Override
	public String toString() {
		return "WatchListDesc [watchListDescKey=" + watchListDescKey + ", watchListName="
				+ watchListName + ", watchListDetails=" + watchListDetails + ", marketDataFrequency="
				+ marketDataFrequency + ", dataProviders=" + dataProviders + ", stockSymbolsList=" + stockSymbolsList
				+ ", operationParameterses=" + operationParameterses + "]";
	}

	@Transient
	@Override
	public Long getId() {
		return getWatchListId();
	}

	@Override
	public void setId(Long id) {
		this.watchListDescKey.setWatchListId(id);
	}

	@Override
	public void setWatchListId(Long watchListId) {
		this.watchListDescKey = new WatchListDescKey(watchListId);
	}

	public List<InstId> getInstIdList() {
		return instIdList;
	}

	public void setInstIdList(List<InstId> instIdList) {
		this.instIdList = instIdList;
	}
	
}

