package com.gjj.igden.model;

public class OperationParameters {
  
	private Long id;
	
	private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }
}