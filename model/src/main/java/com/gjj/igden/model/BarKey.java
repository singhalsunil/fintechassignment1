package com.gjj.igden.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
class BarKey implements Serializable {

	private static final long serialVersionUID = -3618095185677608289L;

	@Column(name = "md_id") 
	Long mdId;

	@Column(name = "instId_fk")
	protected String instId;

	public BarKey() {
	}

	public BarKey(Long mdId, String instId) {
		super();
		this.mdId = mdId;
		this.instId = instId;
	}

	public Long getMdId() {
		return mdId;
	}

	public void setMdId(Long mdId) {
		this.mdId = mdId;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	@Override
	public String toString() {
		return "BarKey [mdId=" + mdId + ", instId=" + instId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((instId == null) ? 0 : instId.hashCode());
		result = prime * result + ((mdId == null) ? 0 : mdId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BarKey other = (BarKey) obj;
		if (instId == null) {
			if (other.instId != null)
				return false;
		} else if (!instId.equals(other.instId))
			return false;
		if (mdId == null) {
			if (other.mdId != null)
				return false;
		} else if (!mdId.equals(other.mdId))
			return false;
		return true;
	}

}