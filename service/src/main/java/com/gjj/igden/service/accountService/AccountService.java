package com.gjj.igden.service.accountService;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.dao.daoimpl.AccountDaoImpl;
import com.gjj.igden.dao.daoimpl.AvatarDaoImpl;
import com.gjj.igden.dao.daoimpl.WatchListDescDaoImpl;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.passwordencoder.AppPasswordEncoder;

@Service
public class AccountService {
	
	static Logger logger = LogManager.getLogger(AccountService.class); 
	
  @Autowired
  private AccountDaoImpl accountDaoImpl;
  
  @Autowired
  private WatchListDescDaoImpl watchListDescDao;
  
  @Autowired AvatarDaoImpl avatarDaoImpl;

  public List<Account> getAccountList() {
	  return accountDaoImpl.readAll();
  }

  public boolean createAccount(Account account) {
	  try {
		  account.setCreationDate(new Date());
		  /*Avatar avatar = new Avatar();
		  avatar.setImage(avatarDaoImpl.getDefaultAvatar());*/
		  //account.setAvatar(null);
		  account.setEnabled(true);
		  account.setPassword(AppPasswordEncoder.generatePassword(account.getPassword(), account.getAccountName()));
		  accountDaoImpl.create(account);
		  logger.debug("Creating account in account service::" + account);
		  return true;
		} catch (DAOException e) {
			logger.error("Account not created" + account);
			throw new RuntimeException("Account not created::", e.getCause());
		}
  }

  public boolean updateAccount(Account account) {
	  Account ac = getAccount(account.getId());
	  logger.debug("Upadating account in account service::" + account);
	  if(null != ac) {
		  ac.setAccountName(account.getAccountName());
		  ac.setAdditionalInfo(account.getAdditionalInfo());
		  ac.setEmail(account.getEmail());
	  }
	  accountDaoImpl.update(ac);
		return true;
  }
  
  public Account getAccount(Long accId) {
	  logger.debug("Retrieving account in account service for id::" + accId);
	  return accountDaoImpl.read(new Account(accId));
  }

  public Account retrieveAccount(Long accId) {
	  logger.debug("Retrieving account with datasets in account service for id::" + accId);
	  Account account = new Account();
	  account = accountDaoImpl.read(new Account(accId));
	  List<WatchListDesc> dataSets = watchListDescDao.getDataSetsAttachedToAcc(accId);
	  account.setDataSets(dataSets);
	  
	  return account;
  }

  public boolean delete(Long id) throws DAOException {
	  logger.debug("Deleting account in account service for id::" + id);	
	  Account account = getAccount(id);
	  accountDaoImpl.delete(account);	
	  return false;
  }

  public boolean setImage(long accId, InputStream is) {
	  logger.debug("Setting image in account service for id::" + accId);
    return accountDaoImpl.setImage(accId, is);
  }

  public byte[] getImage(int accId){
	  logger.debug("Retrieving image in account service for id::" + accId);
    return accountDaoImpl.getImage(accId);
  }
}
