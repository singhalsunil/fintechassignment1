package com.gjj.igden.service.barService;

import com.gjj.igden.model.Bar;
import com.gjj.igden.model.InstId;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.dao.daoimpl.BarDaoImpl;
import com.gjj.igden.dao.daoimpl.InstIdDaoImpl;
import com.gjj.igden.service.accountService.AccountService;
import com.gjj.igden.service.util.ServiceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class BarService {
	
	static Logger logger = LogManager.getLogger(BarService.class); 
	
  @Autowired
  private BarDaoImpl barDao;
  @Autowired
  private InstIdDaoImpl instIdDaoImpl;

  public Set<Bar> getBarList(String instId) {
	  logger.debug("In market data service, Retrieving market data list for symbol::" + instId);
    return barDao.getBarList(instId);
  }

  public Bar getSingleBar(long barId, String instId) {
	  logger.debug("In market data service, Retrieving market data  for symbol::" + instId + barId);
    return barDao.getSingleBar(barId, instId);
  }

  public boolean update(Bar bar) {
	  logger.debug("In market data service, Updating market data  for:: " + bar);
    return barDao.updateBar(bar);
  }

  public boolean createBar(Bar bar) throws ServiceException {
	  logger.debug("In market data service, Creating market data  for ::" + bar);
    try {
      return barDao.createBar(bar);
    } catch (DAOException e) {
    	 logger.error("In market data service, Market data is no created ::" + e);
      throw new ServiceException.ExceptionBuilder().setException(e).build();
    }
  }

  public boolean deleteBar(Bar bar) throws DAOException {
	  logger.debug("In market data service, Deleting market data  for ::" + bar);
    return barDao.deleteBar(bar);
  }

  public List<String> searchTickersByChars(String tickerNamePart) {
	  logger.debug("In market data service, Search Tickers By Chars  ::" + tickerNamePart);
    return instIdDaoImpl.searchTickersByChars(tickerNamePart.trim());
  }
  
  public List<Bar> getFullTickersObjectByChars(String tickerNamePart) {
	  logger.debug("In market data service, Get Full Tickers Object By Chars ::" + tickerNamePart);
	    return barDao.getFullTickersObjectByChars(tickerNamePart.trim());
	  }
  
  public List<Bar> getAllBar() {
	  logger.debug("In market data service, Retrieving all market data ::");
	  return barDao.readAll();
  }
}
