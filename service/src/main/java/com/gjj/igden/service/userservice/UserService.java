package com.gjj.igden.service.userservice;

import com.gjj.igden.dao.UserDao;
import com.gjj.igden.model.Account;
import com.gjj.igden.service.barService.BarService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
	
	static Logger logger = LogManager.getLogger(UserService.class); 

    @Autowired
    UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String accountName) throws UsernameNotFoundException {
    	logger.debug("In UserDetails,load user by username::" + accountName);
        Account account = userDao.findAccountByName(accountName);

        if (account == null) {
            throw new UsernameNotFoundException(String.format("User with username=\"%s\" does not exist", accountName));
        }

        if (!account.isEnabled()) {
            throw new IllegalArgumentException(String.format("User not enabled"));
        }
        return account;
    }
}
