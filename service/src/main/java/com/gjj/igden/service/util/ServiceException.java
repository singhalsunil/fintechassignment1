package com.gjj.igden.service.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("null")
public class ServiceException extends Exception {
	
	static Logger logger = LogManager.getLogger(ServiceException.class); 

	/**
	 * 
	 */
	private static final long serialVersionUID = 7141417278076354128L;
	private StringBuilder additionalMessage = new StringBuilder("");
	private Exception exception;

	public ServiceException(ExceptionBuilder builder) {
		logger.error("ServiceException" + builder);
		additionalMessage = builder.additionalMessageBuilder;
		exception = builder.exception;
	}

	public void printErrMessage() {
		logger.error("ServiceException print error message::" + additionalMessage.toString());
	}

	public void printSimpleMessage() {
		logger.error("ServiceException print simple message::" + additionalMessage.toString());
	}

	public void appendMessage(String message) {
		additionalMessage.append(message).append(" ");
	}

	@Override
	public String getMessage() {
		return additionalMessage.toString();
	}

	@Override
	public void printStackTrace() {
		exception.printStackTrace();
	}

	public static class ExceptionBuilder {

		private StringBuilder additionalMessageBuilder = new StringBuilder("");
		private Exception exception;

		public ExceptionBuilder() {
		}

		public ExceptionBuilder setAdditionalMessage(String additionalMessage) {
			additionalMessageBuilder.append(additionalMessage).append(" ");
			return this;
		}

		public ExceptionBuilder setException(Exception exception) {
			this.exception = exception;
			return this;
		}

		public ServiceException build() {
			return new ServiceException(this);
		}
	}
}