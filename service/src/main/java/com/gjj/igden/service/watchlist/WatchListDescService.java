package com.gjj.igden.service.watchlist;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.dao.daoimpl.AccountDaoImpl;
import com.gjj.igden.dao.daoimpl.InstIdDaoImpl;
import com.gjj.igden.dao.daoimpl.WatchListDescDaoImpl;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.model.WatchListDescKey;
import com.gjj.igden.service.util.ServiceException;

@Service
public class WatchListDescService {
	
	static Logger logger = LogManager.getLogger(ServiceException.class);
	
	@Autowired
	private WatchListDescDaoImpl watchListDescDao;
	
	@Autowired
	private AccountDaoImpl accountDaoImpl;
	
	@Autowired
	private InstIdDaoImpl instIdDaoImpl;
	
	public List<WatchListDesc> getDataSetsAttachedToAcc(Long id) {
		logger.debug("In WatchListDescService, get data set attached to account by id::" + id);
		return watchListDescDao.getDataSetsAttachedToAcc(id);
	}

	public List<String> getStockSymbolsList(Long id) {
		logger.debug("In WatchListDescService, get stock symbol list by id::" + id);
		return watchListDescDao.getAllStockSymbols(id);
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public boolean delete(Long watchListId, Long accountId) throws DAOException {
		logger.debug("In WatchListDescService, Delete watch list where accId is::" + accountId + "and watch list id is::" + watchListId);
		WatchListDesc watchListDesc = watchListDescDao.readByWatchListIdAndAccountId(watchListId, accountId);
		//return watchListDescDao.deleteWatchListDesc(watchListId, accountId);
		return watchListDescDao.deleteWatchListDesc(watchListDesc);
	}

	public boolean delete(WatchListDesc watchListDesc) throws DAOException {
		//instIdDaoImpl.updateInstIdToNull(watchListDesc.getAccount().getId());
		logger.debug("In WatchListDescService, Delete watch list ::" + watchListDesc);
		return watchListDescDao.deleteWatchListDesc(watchListDesc);
	}

	public boolean create(WatchListDesc watchListDesc, Long accId) throws DAOException {
		logger.debug("In WatchListDescService, Create watch list ::" + watchListDesc);
		Account account = accountDaoImpl.read(new Account(accId));
		watchListDesc.setStockSymbolsListFromOperationList(watchListDesc.getOperationParameterses());
		WatchListDescKey watchListDescKey = new WatchListDescKey(account.getId());
		watchListDesc.setWatchListDescKey(watchListDescKey);
		return watchListDescDao.createWatchListDesc(watchListDesc);
	}

	public IWatchListDesc getWatchListDesc(Long dsId, Long accId) {
		logger.debug("In WatchListDescService, Get watch list where data set id is::" + dsId + "and account id is::" + accId);
		return watchListDescDao.getWatchListDesc(dsId, accId);
	}

	public boolean update(IWatchListDesc watchListDesc) {
		return watchListDescDao.updateWatchListDesc(watchListDesc);
	}
}
